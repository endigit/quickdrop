# README #


### What is this repository for? ###

* Quick summary:
This repository is desinged to hold and track useful quick drop tools for LabVIEW. 
* Version
0.2

### How do I get set up? ###

* Summary of set up:
Clone this repo into your labView Quick Drop plugin directory. This is typically at C:\Users\User\Documents\LabVIEW Data\Quick Drop Plugins.
* Configuration:
You might need to check for conflicts of hotkeys if you have assigned custom Quick Drop hotkeys. Other than that it should be plug and play.

### Contribution guidelines ###

* Writing tests:
On top of your standard checks to make sure things work. Check that you aren't causing hotkey conflicts with other plugins. You should also verify that your plugin cancels any transaction in which nothing is changed.
* Other guidelines:
In general, write to your own branch until your code is stable. If you want your changes to go live on the remote push to the Development branch when they are stable, and we will roll them into master.

### Who do I talk to? ###

* Repo owner or admin:
Jake Hansen
* Other community or team contact:
Marty Vowles
* Special thanks to outside contributors:
Karl Muecke - Align to Connector Pane Quick Drop and associated library. 
Darren Nattinger - Disconnect from typedef
